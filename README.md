

# COURS GIT LAB
*29/09/2020*

### Contenu

Cours d'intégration. Utilisation de Gitlab comme utils de versioning.



<!-- Un commentaire de type A -->


**Liste**  

- Création d'un projet sur GitLab <br>
*https://youtu.be/WZNrGVXQLow*
- Récupération de projet Gitlab sur Webstorm <br>
*https://youtu.be/CQ1orWs1_NU*
- Création de branches sur un projet (Gitlab/Webstorm)<br>
*https://youtu.be/rXRP9_7UZBo*
- Les conflits lors de Merge Request<br>
*https://youtu.be/TLSSndfhpNQ*
- Prise de notes en Markown<br>
*https://youtu.be/ihYhfRhs_Mc*

Information finale.